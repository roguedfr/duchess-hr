/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {
	
	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
		  jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
		} else {
		  jQuery(this).wrap("<div class='responsive-embed'/>");
		}
	});
}); 

 jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 100) {
      jQuery("body").addClass("scrolled");
    } else {
      jQuery("body").removeClass("scrolled");
    }
  });

 var $animation_elements = jQuery('.slide-up, .slow-fade');
var $window = jQuery(window);
$window.trigger('scroll');

$window.ready(check_if_in_view);
$window.on('scroll', check_if_in_view);
$window.on('scroll resize', check_if_in_view);

  function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  jQuery.each($animation_elements, function() {
    var $element = jQuery(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      setTimeout(function(){
        $element.addClass('active');
      }, 500);
    }
  });
}

  jQuery('.testimonials__container').slick({
    autoplay: true,
    autoplaySpeed: 8000,
    pauseOnHover:true,
    prevArrow:'<button type="button" class="border__top border__left border__medium-grey slick__button slick__prev"></button>',
    nextArrow:'<button type="button" class="border__bottom border__right border__medium-grey slick__button slick__next"></button>'
  })

  
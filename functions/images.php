<?php 
add_image_size( 'full-width', 1920, 1080, false );
add_image_size( 'services-size', 1200, 675, true );
add_image_size( 'consultation-size', 600, 300, true );
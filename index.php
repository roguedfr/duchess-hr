<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>

<?php
$backgroundImage = get_field('hero_image', 'options');?>

<section class="hero hero-page" style="background-image:url('<?php echo esc_url($backgroundImage['url']);?>');">
	<div class="grid-container">
		<div class="grid-x align-center align-middle h50 text-center pt10">
			<div>
				<?php if (get_field('title', 'options')):?>
					<h1 class="heading__xxl heading__white pb1"><?php the_field('title', 'options');?></h1>
				<?php endif;?>
			</div>
		</div>
	</div>
</section>

<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
				<!-- To see additional archive styles, visit the /parts directory -->
				<?php get_template_part( 'parts/loop', 'archive' ); ?>
			    
			<?php endwhile; ?>	

				<?php joints_page_navi(); ?>
				
			<?php else : ?>
										
				<?php get_template_part( 'parts/content', 'missing' ); ?>
					
			<?php endif; ?>


		</div>
	</div>
</section>

<?php get_footer(); ?>
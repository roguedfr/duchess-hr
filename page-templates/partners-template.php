<?php
/**
 * ============== Template Name: Partners Page
 */
get_header();?>

<?php get_template_part( 'parts/page', 'hero' ); ?>

<?php get_template_part( 'parts/page', 'partners' ); ?>

<?php get_footer(); ?>
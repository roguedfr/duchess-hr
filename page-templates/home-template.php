<?php
/**
 * ============== Template Name: Home
 */
get_header();?>

<?php get_template_part( 'parts/hero', 'home' ); ?>

<?php get_template_part( 'parts/services', 'home');?>

<?php get_template_part( 'parts/software', 'home');?>

<?php get_template_part( 'parts/partners', 'home');?>

<?php get_footer(); ?>
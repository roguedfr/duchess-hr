<?php
/**
 * ============== Template Name: Contact Page
 */
get_header();?>

<?php get_template_part( 'parts/page', 'hero' ); ?>

<section class="pb4 pt4 section__light-grey">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="medium-4 cell show-for-medium">
				<div class="contact__logo mb1 text-center">
					<?php get_template_part( 'parts/nav', 'logo' ); ?>
				</div>
				<h6 class="heading__sm">Duchess HR</h6>
				<p><?php the_field('address', 'options');?></p>
				<p>Telephone - <a href="tel:<?php the_field('telephone_number', 'options');?>" class="heading__primary"><?php the_field('telephone_number', 'options');?></a><br/>
				Email - <a href="mailto:<?php the_field('email', 'options');?>" class="heading__primary"><?php the_field('email', 'options');?></a></p>
			</div>
			<div class="medium-8 cell contact__form">
				<?php echo do_shortcode( '[contact-form-7 id="82" title="Footer Contact"]' ); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
<?php
/**
 * ============== Template Name: Software Page
 */
get_header();?>

<?php get_template_part( 'parts/page', 'hero' ); ?>

<?php get_template_part( 'parts/page', 'software' ); ?>

<?php get_footer(); ?>
<?php
/**
 * ============== Template Name: About Us Page
 */
get_header();?>

<?php get_template_part( 'parts/page', 'hero' ); ?>

<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<?php if(get_field('content')){;?>
				<div class="small-12 cell pb2 pt2 pr2 pl2 section__white">
					<?php the_field('content');?>
				</div>
			<?php };?>
		</div>
	</div>
</section>



<?php get_footer(); ?>
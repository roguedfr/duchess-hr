<?php
/**
 * ============== Template Name: Services Page
 */
get_header();?>

<?php get_template_part( 'parts/page', 'hero' ); ?>

<?php get_template_part( 'parts/page', 'services' ); ?>

<?php get_footer(); ?>
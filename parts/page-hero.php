<?php
$backgroundImage = get_field('hero_image');?>

<section class="hero hero-page" style="background-image:url('<?php echo esc_url($backgroundImage['url']);?>');">
	<div class="grid-container">
		<div class="grid-x align-center align-middle h50 text-center pt10">
			<div class="slide-up">
				<?php if (get_field('title')):?>
					<h1 class="heading__xxl heading__white pb1"><?php the_field('title');?></h1>
				<?php endif;?>
				<?php if (get_field('subheading')):?>
					<h2 class="heading__sm heading__white heading__body-font"><?php the_field('subheading');?></h2>
				<?php endif;?>
				<div class="grid-x grid-padding-x align-center pt2"> 
					<?php 
						$btn = get_field('cta');
						$btn_text = $btn['button_text'];
						$btn_link = $btn['button_link'];
						$btn_type = $btn['button_type'];
						$btn_colour = $btn['button_colour'];
						$btn_consultation = $btn['consultation_button'];
						?>
					<?php if($btn_text){;?>
						<div class="mr1">
							<?php if ($btn_consultation) {;?>
							<button class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>" data-open="consultationModal">
								<?php echo $btn_text;?>
							</button>
							<?php } else {;?>
								<a href="<?php echo $button_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
								<?php echo $btn_text;?>
							</a>
							<?php };?>
						</div>
					<?php };?>
				</div>
			</div>
		</div>
	</div>
</section>
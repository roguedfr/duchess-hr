<section class="section__light-grey pb4 pt4" id="services">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="shrink cell service__logo">
				<?php get_template_part( 'parts/nav', 'logo' ); ?>
			</div>
			<?php if (get_field('services_title')):?>
			<div class="small-12 cell text-center pb1">
				<h3 class="heading__xl mb1"><?php the_field('services_title');?></h3>
			</div>
			<?php endif;?>
			<?php if (get_field('services_subheading')):?>
			<div class="small-12 cell text-center pb1">
				<h4 class="heading__lg mb1"><?php the_field('services_subheading');?></h4>
			</div>
			<?php endif;?>
			<?php if( have_rows('service') ):?>
				<?php while( have_rows('service') ): the_row(); ?>
					<?php $image = get_sub_field('background_image');?>
					<div class="small-12 medium-6 cell pb2 pr2 pt2 pl2 section__white mb2 service slide-up" style="background-image: url(<?php echo esc_url($image['url']);?>);">
						<div class="service__container">
							<h5 class="heading__lg mb1"><?php the_sub_field('title');?></h5>
							<p class="mb2"><?php the_sub_field('excerpt');?></p>
							<a href="<?php the_sub_field('link');?>">Find Out More</a>
						</div>
					</div>
				<?php endwhile;?>
			<?php endif;?>
			<?php if( have_rows('service_buttons') ) {?>
				<div class="grid-x grid-padding-x align-center pt2"> 
					<?php while( have_rows('service_buttons') ) { the_row();?>
						<?php 
							$btn = get_sub_field('button');
							$btn_text = $btn['button_text'];
							$btn_link = $btn['button_link'];
							$btn_type = $btn['button_type'];
							$btn_colour = $btn['button_colour'];
							$btn_consultation = $btn['consultation_button'];
							?>
						<div class="mr1">
							<?php if ($btn_consultation) {;?>
							<button class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>" data-open="consultationModal">
								<?php echo $btn_text;?>
							</button>
							<?php } else {;?>
								<a href="<?php echo $button_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
								<?php echo $btn_text;?>
							</a>
							<?php };?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php
$backgroundImage = get_field('backgroundimage');?>

<section class="hero h100" style="background-image:url('<?php echo esc_url($backgroundImage['url']);?>');">
	<div class="grid-container">
		<div class="grid-x align-center align-middle h100 text-center slide-up">
			<div>
				<?php if (get_field('title')):?>
					<h1 class="heading__xxl heading__white pb1"><?php the_field('title');?></h1>
				<?php endif;?>
				<?php if (get_field('subheading')):?>
					<h2 class="heading__sm heading__white heading__body-font"><?php the_field('subheading');?></h2>
				<?php endif;?>
				<?php if( have_rows('buttons') ) {?>
					<div class="grid-x grid-padding-x align-center pt2"> 
						<?php while( have_rows('buttons') ) {
							the_row();?>
							<?php 
								$btn = get_sub_field('button');
								$btn_text = $btn['button_text'];
								$btn_link = $btn['button_link'];
								$btn_type = $btn['button_type'];
								$btn_colour = $btn['button_colour'];
								?>
							<div class="mr1">
								<a href="<?php echo $btn_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
									<?php echo $btn_text;?>
								</a>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<a class="hero__scroll border__right border__bottom border__light" href="#services"></a>
</section>
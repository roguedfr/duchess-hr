<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<div id="post-<?php the_ID(); ?>" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>)" class="small-12 medium-6 cell pb2 pr2 pt2 pl2 section__white mb2 service">				
	<div class="service__container">
		<h5 class="heading__lg mb1"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
		<p class="mb2"><?php the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); ?></p>
		<a href="<?php the_permalink() ?>">Find Out More</a>
	</div>
</div>
<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="shrink cell service__logo">
				<?php get_template_part( 'parts/nav', 'logo' ); ?>
			</div>
			<?php if (get_field('partners_title')):?>
			<div class="small-12 cell text-center pb1">
				<h3 class="heading__xl mb1"><?php the_field('partners_title');?></h3>
			</div>
			<?php endif;?>
			<?php if (get_field('partners_subheading')):?>
			<div class="small-12 cell text-center pb1">
				<h4 class="heading__lg mb1"><?php the_field('partners_subheading');?></h4>
			</div>
			<?php endif;?>
			<?php if( have_rows('partners') ):?>
			<div class="small-12 cell pb2">
				<div class="partners__logo-container text-center">
					<?php while( have_rows('partners') ): the_row(); ?>
						<?php if(get_sub_field('type') == "client"){;?>
						<?php $logo = get_sub_field('logo');?>
						<?php if( get_sub_field('website_link') ){?>
						<a href="<?php the_sub_field('website_link');?>" target="_blank" class="slow-fade">
						<?php };?>
							<img src="<?php echo $logo['url'];?>" alt="<?php the_sub_field('company_name');?>"/>
						<?php if( get_sub_field('website_link') ){?>
						</a>
						<?php };?>
						<?php };?>
					<?php endwhile;?>
				</div>
				<h3 class="heading__xl text-center pt4">Our Partners</h3>
				<div class="partners__logo-container text-center">
					<?php while( have_rows('partners') ): the_row(); ?>
						<?php if(get_sub_field('type') == "partner"){;?>
						<?php $logo = get_sub_field('logo');?>
						<?php if( get_sub_field('website_link') ){?>
						<a href="<?php the_sub_field('website_link');?>" target="_blank" class="slow-fade">
						<?php };?>
							<img src="<?php echo $logo['url'];?>" alt="<?php the_sub_field('company_name');?>"/>
						<?php if( get_sub_field('website_link') ){?>
						</a>
						<?php };?>
						<?php };?>
					<?php endwhile;?>
				</div>
			</div>
			<?php endif;?>
			<?php if( have_rows('testimonials') ):?>
				<div class="section__white pb2 pl5 pr5 pt2 cell small-12">
					<?php if (get_field('partners_title')):?>
						<div class="small-12 cell text-center pb1">
							<h3 class="heading__xl mb1"><?php the_field('testimonials_title');?></h3>
						</div>
					<?php endif;?>
				<div class="testimonials__container">
					<?php while( have_rows('testimonials') ): the_row(); ?>
						<div>
							<h3 class="heading__md mb1"><?php the_sub_field('title');?></h3>
							<?php the_sub_field('content');?>
							<div class="text-right font700"><?php the_sub_field('credit');?></div>
						</div>
					<?php endwhile;?>
				</div>
				</div>
			<?php endif;?>
			<?php 
				$btn = get_field('partners_button');
				$btn_text = $btn['button_text'];
				$btn_link = $btn['button_link'];
				$btn_type = $btn['button_type'];
				$btn_colour = $btn['button_colour'];
				$btn_consultation = $btn['consultation_button'];
				?>
			<div class="mt2">
				<?php if ($btn_consultation) {;?>
				<button class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>" data-open="consultationModal">
					<?php echo $btn_text;?>
				</button>
				<?php } else {;?>
					<a href="<?php echo $button_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
					<?php echo $btn_text;?>
				</a>
				<?php };?>
			</div>
		</div>
	</div>
</section>
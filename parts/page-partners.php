<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<?php if( have_rows('testimonials') ):?>
				<div class="section__white pb2 pl5 pr5 pt2 cell small-12 slide-up">
					<?php if (get_field('testimonials_title')):?>
						<div class="small-12 cell text-center pb1">
							<h3 class="heading__xl mb1"><?php the_field('testimonials_title');?></h3>
						</div>
					<?php endif;?>
				<div class="testimonials__container">
					<?php while( have_rows('testimonials') ): the_row(); ?>
						<div>
							<h3 class="heading__md mb1"><?php the_sub_field('title');?></h3>
							<?php the_sub_field('content');?>
							<div class="text-right font700"><?php the_sub_field('credit');?></div>
						</div>
					<?php endwhile;?>
				</div>
				</div>
			<?php endif;?>
			<?php if( have_rows('partners') ):?>
			<div class="small-12 cell pt4 pb2">
				<h3 class="heading__xl text-center">Our Clients</h3>
				<div class="partners__logo-container text-center">
					<?php while( have_rows('partners') ): the_row(); ?>
						<?php if(get_sub_field('type') == "client"){;?>
						<?php $logo = get_sub_field('logo');?>
						<?php if( get_sub_field('website_link') ){?>
						<a href="<?php the_sub_field('website_link');?>" target="_blank" class="slow-fade">
						<?php };?>
							<img src="<?php echo $logo['url'];?>" alt="<?php the_sub_field('company_name');?>"/>
						<?php if( get_sub_field('website_link') ){?>
						</a>
						<?php };?>
						<?php };?>
					<?php endwhile;?>
				</div>
				<h3 class="heading__xl text-center pt4">Our Partners</h3>
				<div class="partners__logo-container text-center">
					<?php while( have_rows('partners') ): the_row(); ?>
						<?php if(get_sub_field('type') == "partner"){;?>
						<?php $logo = get_sub_field('logo');?>
						<?php if( get_sub_field('website_link') ){?>
						<a href="<?php the_sub_field('website_link');?>" target="_blank" class="slow-fade">
						<?php };?>
							<img src="<?php echo $logo['url'];?>" alt="<?php the_sub_field('company_name');?>"/>
						<?php if( get_sub_field('website_link') ){?>
						</a>
						<?php };?>
						<?php };?>
					<?php endwhile;?>
				</div>
			</div>
			<?php endif;?>
		</div>
	</div>
</section>
<?php $image = get_field('software_background');?>
<section class="section__primary software">
	<div class="software__background" style="background-image:url(<?php echo esc_url($image['url']);?>);"></div>
	<div class="grid-container">
		<div class="grid-x">
			<div class="small-12 large-7 pb4 pt4 text-center">
				<?php if (get_field('software_title')):?>
				<h3 class="heading__xl heading__white mb1"><?php the_field('software_title');?></h3>
				<?php endif;?>
				<?php if (get_field('software_contact')):?>
				<div class="heading__white mb1">
					<?php the_field('software_contact');?>
				</div>
				<?php endif;?>
				<?php if( have_rows('software_features') ):?>
				<div class="grid-x grid-margin-x">
					<div class="small-10 small-offset-1">
						<div class="grid-x grid-margin-x">
							<?php while( have_rows('software_features') ): the_row(); ?>
								<div class="small-6 medium-4 cell border__top border__bottom border__left border__right border__secondary pb1 pt1 pl1 pr1 mb1 slow-fade">
									<?php if (get_sub_field('icon')):?>
										<?php $icon = get_sub_field('icon');?>
										<img src="<?php echo esc_url($icon['url']);?>" alt="<?php the_sub_field('title');?>" class="software__icon mb1"/>
									<?php endif;?>
									<p class="heading__white"><?php the_sub_field('title');?></p>
								</div>
							<?php endwhile;?>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if( have_rows('software_buttons') ) {?>
				<div class="grid-x grid-padding-x align-center pt2"> 
					<?php while( have_rows('software_buttons') ) { the_row();?>
						<?php 
							$btn = get_sub_field('button');
							$btn_text = $btn['button_text'];
							$btn_link = $btn['button_link'];
							$btn_type = $btn['button_type'];
							$btn_colour = $btn['button_colour'];
							$btn_consultation = $btn['consultation_button'];
							?>
						<div class="mr1">
							<?php if ($btn_consultation) {;?>
							<button class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>" data-open="consultationModal">
								<?php echo $btn_text;?>
							</button>
							<?php } else {;?>
								<a href="<?php echo $button_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
								<?php echo $btn_text;?>
							</a>
							<?php };?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>
<div class="reveal modal" id="consultationModal" data-reveal>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
  <?php
		$image = get_field('consultation_image', 'options');
		$size = 'consultation-size';
		$imageDetails = $image['sizes'][ $size ];?>
	<div class="modal__image" style="background-image: url(<?php echo esc_url($imageDetails);?>);">
		<h3 class="heading__white heading__xl"><?php the_field('consultation_title', 'options');?></h3>
	</div>
	<div class="pb2 pt2 pl2 pr2">
		<span class="heading__sm text-center"><?php the_field('consultation_text', 'options');?></span>
		<?php echo do_shortcode( '[contact-form-7 id="211" title="Consultation Form"]' ); ?>
	</div>
</div>
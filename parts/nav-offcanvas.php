<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div id="off-canvas-bar-menu" class="hide-for-large">
	<div class="grid-x">
		<div class="cell small-9 small-offset-1 mb1">
			<a href="<?php echo home_url(); ?>" class="off-canvas-logo">
				<?php get_template_part( 'parts/nav', 'logo' ); ?>
			</a>
		</div>
		<div class="cell small-2 mt1">
			<button class="menu-icon" type="button" data-toggle="off-canvas"></button>
		</div>
	</div>
</div>
<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="off-canvas position-right" id="off-canvas" data-off-canvas>

	<button class="close-button heading__white" aria-label="Close menu" type="button" data-close>
	  <span aria-hidden="true">&times;</span>
	</button>

	<div class="grid-x align-center mt2">
		<div class="small-4">
			<a href="<?php echo home_url(); ?>" >
				<?php get_template_part( 'parts/nav', 'logo' ); ?>
			</a>
		</div>
		<div class="small-9 mt2 text-center">
			<?php joints_off_canvas_nav(); ?>
		</div>
		<div class="small-9 mt2 text-center">
			<a href="tel:<?php the_field('telephone_number', 'options');?>" class="heading__white heading__sm">Tel: <?php the_field('telephone_number', 'options');?></a><br/>
			<a href="mailto:<?php the_field('email', 'options');?>" class="heading__white heading__sm">Email: <?php the_field('email', 'options');?></a>
		</div>
		<div class="small-9 mt2 text-center off-canvas__social">
		<?php if(get_field('facebook','options')){ ;?>
				<a href="<?php the_field('facebook','options');?>" target="_blank">
					<?php get_template_part( 'parts/icon', 'facebook' ); ?>
				</a>
			<?php };?>
			<?php if(get_field('twitter','options')){ ;?>
				<a href="<?php the_field('twitter','options');?>" target="_blank">
					<?php get_template_part( 'parts/icon', 'twitter' ); ?>
				</a>
			<?php };?>
			<?php if(get_field('instagram','options')){ ;?>
				<a href="<?php the_field('instagram','options');?>" target="_blank">
					<?php get_template_part( 'parts/icon', 'instagram' ); ?>
				</a>
			<?php };?>
			<?php if(get_field('pinterest','options')){ ;?>
				<a href="<?php the_field('pinterest','options');?>" target="_blank">
					<?php get_template_part( 'parts/icon', 'pinterest' ); ?>
				</a>
			<?php };?>
			<?php if(get_field('linkedin','options')){ ;?>
				<a href="<?php the_field('linkedin','options');?>" target="_blank">
					<?php get_template_part( 'parts/icon', 'linkedin' ); ?>
				</a>
			<?php };?>
		</div>
	</div>
	

	<?php if ( is_active_sidebar( 'offcanvas' ) ) : ?>

		<?php dynamic_sidebar( 'offcanvas' ); ?>

	<?php endif; ?>

</div>

<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/responsive-navigation/
 */
?>
<div class="section__black">
	<div class="grid-container full">
		<div class="grid-x grid-padding-x align-right pl1 pr1">
			<div class="cell auto header__social show-for-medium">
				<?php if(get_field('facebook','options')){ ;?>
					<a href="<?php the_field('facebook','options');?>" target="_blank">
						<?php get_template_part( 'parts/icon', 'facebook' ); ?>
					</a>
				<?php };?>
				<?php if(get_field('twitter','options')){ ;?>
					<a href="<?php the_field('twitter','options');?>" target="_blank">
						<?php get_template_part( 'parts/icon', 'twitter' ); ?>
					</a>
				<?php };?>
				<?php if(get_field('instagram','options')){ ;?>
					<a href="<?php the_field('instagram','options');?>" target="_blank">
						<?php get_template_part( 'parts/icon', 'instagram' ); ?>
					</a>
				<?php };?>
				<?php if(get_field('pinterest','options')){ ;?>
					<a href="<?php the_field('pinterest','options');?>" target="_blank">
						<?php get_template_part( 'parts/icon', 'pinterest' ); ?>
					</a>
				<?php };?>
				<?php if(get_field('linkedin','options')){ ;?>
					<a href="<?php the_field('linkedin','options');?>" target="_blank">
						<?php get_template_part( 'parts/icon', 'linkedin' ); ?>
					</a>
				<?php };?>
			</div>
			<div class="cell shrink">
				<a href="tel:<?php the_field('telephone_number', 'options');?>" class="heading__white heading__xs header__icon"><?php get_template_part( 'parts/icon', 'phone' ); ?><?php the_field('telephone_number', 'options');?></a>
			</div>
			<div class="cell shrink">
				<a href="mailto:<?php the_field('email', 'options');?>" class="heading__white heading__xs header__icon"><?php get_template_part( 'parts/icon', 'email' ); ?> <?php the_field('email', 'options');?></a>
			</div>
		</div>
	</div>
</div>
<div id="main-menu">
	<div class="grid-container full">
		<div class="grid-x grid-padding-x align-middle show-for-large">
			<div class="cell small-2">
				<a href="<?php echo home_url(); ?>" class="logo">
					<?php get_template_part( 'parts/nav', 'logo' ); ?>
				</a>
			</div>
			<div class="cell small-8 text-center">
				<?php joints_top_nav(); ?>
			</div>
			<div class="cell small-2 text-right">
				<?php 
					$btn = get_field('header_button', 'options');
					$btn_text = $btn['button_text'];
					$btn_link = $btn['button_link'];
					$btn_type = $btn['button_type'];
					$btn_colour = $btn['button_colour'];
					$btn_consultation = $btn['consultation_button'];
					?>
				<div class="mr1">
					<?php if ($btn_consultation) {;?>
					<button class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>" data-open="consultationModal">
						<?php echo $btn_text;?>
					</button>
					<?php } else {;?>
						<a href="<?php echo $button_link;?>" class="mb0 button <?php echo $btn_type;?> <?php echo $btn_colour;?>">
						<?php echo $btn_text;?>
					</a>
					<?php };?>
				</div>
			</div>
		</div>
	</div>
</div>
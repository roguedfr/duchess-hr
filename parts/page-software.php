<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<?php if(get_field('content')){;?>
				<div class="small-8 cell text-center">
					<?php the_field('content');?>
				</div>
			<?php };?>
		</div>
	</div>
</section>

<?php if( have_rows('software_features') ):?>
<section class="pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="small-12">
				<div class="grid-x grid-margin-x align-center">
					<div class="cell small-12 text-center">
						<h3><?php the_field('software_features_title');?></h3>
					</div>
					<?php while( have_rows('software_features') ): the_row(); ?>
						<div class="small-10 medium-6 large-4 cell mb1 text-center slow-fade">
							<?php if (get_sub_field('icon')):?>
								<?php $icon = get_sub_field('icon');?>
								<img src="<?php echo esc_url($icon['url']);?>" alt="<?php the_sub_field('title');?>" class="software__icon mb1"/>
							<?php endif;?>
							<h6 class="heading__sm"><?php the_sub_field('title');?></h6>
							<div>
								<?php the_sub_field('info');?>
							</div>
						</div>
					<?php endwhile;?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
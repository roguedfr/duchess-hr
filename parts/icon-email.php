<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="170px"
	 height="170px" viewBox="0 0 170 170" enable-background="new 0 0 170 170" xml:space="preserve">
<g id="Background">
</g>
<g id="Circle">
	<g>
		<path fill="#FFFFFF" d="M85,170c-46.869,0-85-38.131-85-85S38.131,0,85,0s85,38.131,85,85S131.869,170,85,170z"/>
	</g>
</g>
<g id="TEXT">
</g>
<g id="Icons">
	<g>
		<g>
			<path fill="none" stroke="#282D33" stroke-width="4" stroke-linejoin="round" d="M100.261,48.221l-2.189-1.866l-8.965-7.648
				c-1.918-1.633-5.059-1.633-6.975,0l-8.967,7.648l-1.539,1.313"/>
			<polyline fill="none" stroke="#282D33" stroke-width="4" stroke-linejoin="round" points="48.553,67.347 38.948,75.538 
				38.917,75.538 38.917,75.565 38.907,75.573 38.917,75.573 38.917,129.481 132.31,129.481 132.31,75.573 132.331,75.573 
				132.31,75.554 132.31,75.538 132.288,75.538 122.536,67.22 			"/>
			<polyline fill="none" stroke="#282D33" stroke-width="4" points="48.544,77.638 48.544,48.214 122.228,48.214 122.228,77.638 			
				"/>
			<g>
				<polyline fill="none" stroke="#282D33" stroke-width="4" stroke-linejoin="round" points="132.558,77.487 100.552,103.649 
					70.683,103.649 38.798,77.487 				"/>
				<g>
					<path fill="none" stroke="#282D33" stroke-width="4" stroke-linejoin="round" d="M41.688,128.754l10.391-8.853l7.84-6.744
						c0.922-0.789,2.129-1.83,3.338-2.871l7.766-6.675"/>
					<path fill="none" stroke="#282D33" stroke-width="4" stroke-linejoin="round" d="M100.214,103.611l7.766,6.675
						c1.207,1.041,2.416,2.082,3.336,2.871l7.844,6.744l11.18,9.61"/>
				</g>
			</g>
		</g>
		<g>
			<path fill="none" stroke="#282D33" stroke-width="4" d="M96.064,88.76c-2.743,2.074-6.159,3.307-9.863,3.307
				c-9.04,0-16.365-7.327-16.365-16.364c0-9.039,7.325-16.364,16.365-16.364c5.102,0,9.658,2.336,12.661,5.997"/>
			<path fill="none" stroke="#282D33" stroke-width="4" d="M91.359,68.065c0,0-6.722-1.533-10.614,2.123
				c-4.684,4.399-3.67,11.243,0.707,12.502c4.307,1.238,5.955-2.066,6.457-3.511C88.411,77.736,91.359,68.065,91.359,68.065z"/>
			<path fill="none" stroke="#282D33" stroke-width="4" d="M90.889,69.717c0,0-2.879,10.099-0.886,11.912
				c5.839,5.307,19.284-8.493,6.192-18.931"/>
		</g>
	</g>
</g>
</svg>

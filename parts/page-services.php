<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="small-12 cell show-for-medium slow-fade text-right">
				<button class="button hollow service__dropdown" type="button" data-toggle="services-dropdown">Our Services <?php get_template_part( 'parts/icon', 'chevron-down' ); ?></button>
				<div class="dropdown-pane" id="services-dropdown" data-dropdown data-position="bottom" data-alignment="left">
				  	<?php if( have_rows('service') ) {?>
						<ul class="vertical tabs text-left" data-tabs id="service-tabs">
						<?php while( have_rows('service') ) { the_row();?>
							<li class="tabs-title <?php if (get_row_index() == 1){; ?>is-active<?php };?>"><a href="#softwareTab-<?php echo get_row_index(); ?>"><?php the_sub_field('title');?></a></li>
						<?php } ?>
						</ul>
					<?php } ?>
				</div>
			</div>
			<div class="small-12 cell show-for-medium slide-up">
				<?php if( have_rows('service') ) {?>
					<div class="tabs-content vertical" data-tabs-content="service-tabs">
						<?php while( have_rows('service') ) { the_row();?>
							<?php
							$image = get_sub_field('image');
							$size = 'services-size';
							$imageDetails = $image['sizes'][ $size ];?>
							<div class="tabs-panel <?php if (get_row_index() == 1){; ?>is-active<?php };?>" id="softwareTab-<?php echo get_row_index(); ?>">
								<img src="<?php echo esc_url($imageDetails);?>"/>
								<div class="mt2 pb2 pl2 pr2"><?php the_sub_field('info');?></div>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<?php if( have_rows('service') ) {?>
			<div class="cell small-12 hide-for-medium">
				<ul class="accordion" data-accordion>
					<?php while( have_rows('service') ) { the_row();?>
						<li class="accordion-item <?php if (get_row_index() == 1){; ?>is-active<?php };?>" data-accordion-item>
							 <a href="#" class="accordion-title"><?php the_sub_field('title');?></a>
							<div class="accordion-content" data-tab-content>
								<?php
									$image = get_sub_field('image');
									$size = 'services-size';
									$imageDetails = $image['sizes'][ $size ];?>
									<img src="<?php echo esc_url($imageDetails);?>"/>
									<div class="mt2 pb2 pl2 pr2"><?php the_sub_field('info');?></div>
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
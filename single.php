<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>


<section class="hero hero-page" style="background-image:url('<?php the_post_thumbnail_url('full'); ?>');">
	<div class="grid-container">
		<div class="grid-x align-center align-middle h50 text-center pt10">
			<div>
				<h1 class="heading__xxl heading__white pb1"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section class="section__light-grey pb4 pt4">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
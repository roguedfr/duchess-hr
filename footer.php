<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="section__primary heading__white footer mt0" role="contentinfo">

					<div class="grid-container">
					
						<div class="inner-footer grid-x grid-margin-x grid-padding-x pt2">
							
							<div class="small-12 large-3 mb2 cell">
								<a href="<?php echo home_url(); ?>" class="footer__logo">
									<?php get_template_part( 'parts/nav', 'logo' ); ?>
								</a>
							</div>
							<div class="small-12 large-3 mb2 cell">
								<h6 class="heading__sm">Duchess HR</h6>
								<p><?php the_field('address', 'options');?></p>
								<p>Telephone - <a href="tel:<?php the_field('telephone_number', 'options');?>" class="heading__white"><?php the_field('telephone_number', 'options');?></a><br/>
								Email - <a href="mailto:<?php the_field('email', 'options');?>" class="heading__white"><?php the_field('email', 'options');?></a></p>
							</div>
							<div class="small-12 large-6 mb2 cell">
								<?php echo do_shortcode( '[contact-form-7 id="82" title="Footer Contact"]' ); ?>
							</div>
							<div class="small-12 cell mb2 text-right footer__icons">
								<?php if(get_field('facebook','options')){ ;?>
									<a href="<?php the_field('facebook','options');?>" target="_blank">
										<?php get_template_part( 'parts/icon', 'facebook' ); ?>
									</a>
								<?php };?>
								<?php if(get_field('twitter','options')){ ;?>
									<a href="<?php the_field('twitter','options');?>" target="_blank">
										<?php get_template_part( 'parts/icon', 'twitter' ); ?>
									</a>
								<?php };?>
								<?php if(get_field('instagram','options')){ ;?>
									<a href="<?php the_field('instagram','options');?>" target="_blank">
										<?php get_template_part( 'parts/icon', 'instagram' ); ?>
									</a>
								<?php };?>
								<?php if(get_field('pinterest','options')){ ;?>
									<a href="<?php the_field('pinterest','options');?>" target="_blank">
										<?php get_template_part( 'parts/icon', 'pinterest' ); ?>
									</a>
								<?php };?>
								<?php if(get_field('linkedin','options')){ ;?>
									<a href="<?php the_field('linkedin','options');?>" target="_blank">
										<?php get_template_part( 'parts/icon', 'linkedin' ); ?>
									</a>
								<?php };?>

							</div>
							
							<div class="small-12 large-6 cell pt1">
								<p class="source-org copyright heading__xs">Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved</p>
							</div>
							<div class="small-12 large-6 cell pt1">
								<p class="text-right heading__xs">Design and built by <a href="https://www.ukdigitalmarketing.co.uk/" target="_blank" class="heading__white">UK Digital Marketing</a></p>
							</div>
						
						</div> <!-- end #inner-footer -->

					</div>
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->

		 <?php get_template_part( 'parts/modal', 'consultation' ); ?>
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->